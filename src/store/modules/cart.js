
const state = {
  client: {
    client_account: '',
    contact: '',
    shipping_date: '',
    order_date: new Date().toLocaleString('es-419'),
    shipping_type: ''
  },
  notes: {
    orders: '',
    pendings: ''
  },
  orders: [
    // {
      // patient_case: '4765',
      // product_name:'Exp Col',
      // graduation: '+2.00',
      // color: 'Jade',
      // box: 1,
      // blister: 0,
    // }
  ],
  pendings: [
    // {
      // patient_case: 'Lolo',
      // product_name: 'Biofinity Torix XR',
      // graduation: '-7.50',
      // cylinder: '-3.75',
      // axis: '45º',
      // box: 1,
      // blister: 1
    // }
  ]
}

const getters = {
    orders: state => state.orders,
    client: state => state.client,
    pendings: state => state.pendings,
    notes: state => state.notes
}

const actions = {
    addNewItem({ commit }, payload) {
      commit('addItem', payload)
      /*
        payload = {
          ...product paramas (prod_name, grad, etc),
          order_type: order || pending
      */
    },
    deleteItem({ commit }, payload) {
      commit('deleteItem', payload)
      /*
        payload = {
          index: indef of the array,
          order_type: order || pending
      */
    },
    cleanItems({commit}) {
      commit('cleanItems')
    },
    addClient({commit}, {account}) {
      commit('addClient', {account})
    },
    addContact({commit}, contact) {
      commit('addContact', contact)
    },
    addNotes({commit}, payload) {
      commit('addNotes', payload)
      /*
        payload = {
          notes: notes from the textarea
          order_type: order || pending
      */
    },
    setDate({commit}, shipping_date) {
      commit('setDate', shipping_date)
    },
    setShippingType({commit}, shipping_type) {
      commit('setShippingType', shipping_type)
    }
}

const mutations= {
    // setItems: (state, items) => (state.items = items),
    addItem: (state, payload) => {
      state[payload.order_type].push(payload)
    },
    deleteItem: (state, payload) => state[payload.order_type].splice(payload.index, 1),
    cleanItems: (state) => {
      state.orders = [],
      state.pendings = []
      state.client = {
        client_account: '',
        contact: '',
        shipping_date: '',
        order_date: new Date().toLocaleString('es-419'),
        shipping_type: '',
      },
      state.notes = {
        order: '',
        pending: ''
      }
    },
    addClient: (state, {account}) => {
      state.client.client_account = account
    },
    addContact: (state, contact) => state.client.contact = contact,
    addNotes: (state, payload) => {
      state.notes[payload.order_type] = payload.notes
    },
    setDate: (state, shipping_date) => state.client.shipping_date = shipping_date,
    setShippingType: (state, shipping_type) => state.client.shipping_type = shipping_type
    
}

export default {
    state,
    getters,
    actions,
    mutations
}