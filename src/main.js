import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import 'vuetify/dist/vuetify.min.css'
import store from './store'
import vuetify from './plugins/vuetify';
import VueRouter from 'vue-router'

import Purchase from './purchaseModule/Purchase'
import Clients from './clientsModule/Clients'
import Stock from './stockModule/Stock'
import Pendings from './pendingModule/Pendings'
import Orders from './ordersModule/Orders'
import Shippings from './shippingModule/Shippings'

Vue.use(VueRouter)

Vue.config.productionTip = false

const routes = [
  {path: '/compras', component: Purchase},
  {path: '/clientes', component: Clients},
  {path: '/stock', component: Stock},
  {path: '/pendientes', component: Pendings},
  {path: '/ordenes', component: Orders},
  {path: '/embarques', component: Shippings},
  {path: '*', redirect: '/compras'}
]

const router = new VueRouter({
  routes
})

new Vue({
  store,
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')

