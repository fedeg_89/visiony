const express = require('express')
const app = express()
const path = require('path');
var cors = require('cors')
 
app.use(cors())

// Sends static files  from the public path directory
app.use(express.static(path.join(__dirname, '/public')))
app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded


var cart = require('./routes')
//  Use routes defined in Route.js and prefix with todo
app.use('/api/cart', cart)


app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '../public/index.html'))
});



const port = 3000
app.listen(port, () => console.log(`Visiony listening on port ${port}!`))