// 'use strict'
const express = require('express')
const cart = express.Router()

const mysql = require('mysql');
const connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'visiony'
});
 
connection.connect();

// connection.end();

// get all products' data from the db
cart.route('/getProductsData').get(function (req, res) {
  connection.query('SELECT * from products', function (error, results) {
    if (error) throw error;
    res.json(results)
  })
})

// get all clients' data from the db
cart.route('/getClientsData').get(function (req, res) {
  connection.query('SELECT * from clients', function (error, results) {
    if (error) throw error;
    res.json(results)
  })
})


cart.route('/createOrder').post(async function (req, res) {
  // Checks if there are order
  if (req.body.order[0]) {
    let order = Object.create(req.body.client)
    order.notes = req.body.notes.orders
    order.order_type = 'Orden'
    await connection.beginTransaction(function(err) {
      if (err) throw err
      // Insert Order
      connection.query('INSERT INTO orders SET ?', order, function(error, orderRow) {
        if (error) {
          return connection.rollback(function() {
            throw error;
          });
        }
        console.log('O1')
        req.body.order.forEach(item => {
          item.order_id = orderRow.insertId;
          let stockTable = `${item.family}_${item.type}_stock`

          connection.query(`UPDATE ${stockTable} SET box = box - ${item.box}, blister = blister - ${item.blister} WHERE id = ${item.id}`, (error, itemAdded) => {
            if (error) {
              return connection.rollback(function() {
                throw error;
              });
            }
            delete item.family
            delete item.type
            delete item.id
            console.log(item)
            connection.query('INSERT INTO orders_detail SET ?', item, function (error, row) {
              if (error) {
                return connection.rollback(function() {
                  throw error;
                });
              }
              console.log('O2')
            })
          })
        })
        connection.commit(function(err) {
          if (err) {
            return connection.rollback(function() {
              throw err;
            });
          }
          console.log('O3')
        });
      });
    })
  }

  if (req.body.pending[0]) {
    let pending = Object.create(req.body.client)
    pending.order_type = await 'Pendiente'
    pending.notes = req.body.notes.pendings
    pending.shipping_date = null
    pending.deliver = false
    await connection.beginTransaction(function(err) {
      if (err) throw err
      connection.query('INSERT INTO orders SET ?', pending, function(error, pendingRow) {
        if (error) {
          return connection.rollback(function() {
            throw error;
          });
        }
        console.log('P1')
    
        req.body.pending.forEach(item => {
          item.order_id = pendingRow.insertId;

          if (item.reserved) {
            connection.query(`UPDATE shippings_detail SET box_available = box_available - ${item.box}, SET blister_available = blister_available - ${item.blister} WHERE family = ${item.family} AND lens_type = ${item.type} AND product_id = ${item.id} AND shipping_id = (SELECT id FROM shippings LIMIT 1 ORDER BY id DESC)`,(error, results) => {
              if (error) throw error
            })
          }

          // let stockTable = `${item.family}_${item.type}_stock`

          // connection.query(`UPDATE ${stockTable} SET box = box - ${item.box}, blister = blister - ${item.blister} WHERE id = ${item.id}`, (error, itemAdded) => {
          //   if (error) {
          //     return connection.rollback(function() {
          //       throw error;
          //     });
          //   }
          delete item.family
          delete item.type
          delete item.id
          connection.query('INSERT INTO orders_detail SET ?', item, function (error, row) {
            if (error) {
              return connection.rollback(function() {
                throw error;
              });
            }
            console.log('O2')
          })
        })
      })
      connection.commit(function(err) {
        if (err) {
          return connection.rollback(function() {
            throw err;
          });
        }
        console.log('O3')
      });
    })
  }
  await res.json({res: 'ok'})
})

cart.route('/findShipping').get((req, res) => {
  console
  const family = req.body.family
  const type = req.body.type
  const id = req.body.id
  connection.query(`SELECT * FROM shippings_detail WHERE family = ${family} AND lens_type = ${type} AND product_id = ${id} AND shipping_id = (SELECT id FROM shippings LIMIT 1 ORDER BY id DESC)`), (error, results) => {
    if (error) throw error
    console.log(results)
    results ? res.json(true) : res.json(false)
  }
})

cart.route('/getOrders').get((req, res) => { //?account=client_account
  let response = {}
  const account = req.query.account
  connection.query(`SELECT * FROM orders WHERE client_account = ${account}`, (error, orders) => {
    if (error) throw error
    response.ordersData = orders

    connection.query(`SELECT * FROM orders_detail WHERE order_id IN (SELECT order_id FROM orders WHERE client_account = ${account})`, (error, items) => {
      if (error) throw error
      response.items = items
      res.json(response)
    })
  })
})

cart.route('/getOrdersByDate').get(async (req, res) => { //? when = '>=', date = new Date,  to = when, date2 = date
  // Sets default values
  let when = req.query.when ? req.query.when : '>='
  let date = req.query.date ? new Date(req.query.date) : new Date()
  let to = req.query.to ? req.query.to : when
  let date2 = req.query.date2 ? new Date(req.query.date2) : date

  let response = {}


  const columns = 'clients.nickname, orders.*, sum(orders_detail.box) as boxes, sum(orders_detail.blister) as blisters FROM orders'
  const joins = 'LEFT JOIN orders_detail on orders.order_id = orders_detail.order_id LEFT JOIN clients ON orders.client_account = clients.account'
  const condition = `shipping_date ${when} ${connection.escape(date)} AND shipping_date ${to} ${connection.escape(date2)}`

  connection.query(`SELECT ${columns} ${joins} WHERE ${condition} GROUP BY order_id`, (error, orders) => {
    if (error) throw error
    response.orders = orders

    connection.query(`SELECT * FROM orders_detail WHERE order_id IN (
      SELECT order_id FROM orders WHERE ${condition})`, (error, items) => {
        if (error) throw error
        response.items = items
        res.json(response)
    })
  })
})

cart.route('/getHold').get((req, res) => {
  let response = {}

  const columns = 'clients.nickname, orders.*, sum(orders_detail.box) as boxes, sum(orders_detail.blister) as blisters FROM orders'
  const joins = 'LEFT JOIN orders_detail on orders.order_id = orders_detail.order_id LEFT JOIN clients ON orders.client_account = clients.account'
  let condition = 'orders.deliver = false AND orders.order_type = "Orden"'

  connection.query(`SELECT ${columns} ${joins} WHERE ${condition} GROUP BY order_id`, (error, orders) => {
    if (error) throw error
    response.ordersData = orders
    connection.query(`SELECT * FROM orders_detail WHERE order_id IN (
      SELECT order_id FROM orders WHERE deliver = false AND order_type = "Orden")`, (error, items) => {
        if (error) throw error
        response.items = items

        res.json(response)
      }) 
  })
})

cart.route('/dispatch').patch((req, res) => { // body: value = true/false, id= order_id
  connection.query(`UPDATE orders SET dispatched = ${req.body.value} WHERE order_id = ${req.body.id}`, (error, response) => {
    if (error) throw error
    console.log(response)
    res.json({res: 'ok'})
  })
})

cart.route('/getPendings').get((req, res) => {
  let response = {}
  connection.query(`SELECT * FROM orders WHERE order_type = 'Pendiente'`, (error, pendings) => {
    if (error) throw error
    response.ordersData = pendings
    connection.query(`SELECT * FROM orders_detail WHERE order_id IN (SELECT order_id FROM orders WHERE order_type = 'Pendiente')`, (error, items) => {
      if (error) throw error
      response.items = items

      res.json(response)
    })
  })
})

cart.route('/getStock').get((req, res) => { // ?family=family & type=type
  let stockTable = `${req.query.family}_${req.query.type}_stock`

  connection.query(`SELECT * FROM ${stockTable}`, (error, stock) => {
    if (error) throw error
    res.json(stock)
  })
})

cart.route('/getShippings').get((req, res) => {
  let response = {}
  connection.query(`SELECT * FROM shippings`, (error, shippings) => {
    if (error) throw error
    response.shippingsData = shippings
    connection.query(`SELECT * FROM shippings_detail`, (error, items) => {
      if (error) throw error
      response.items = items

      res.json(response)
    })
  })
})

// let biom_tor = []
//         for (let grad = -3; grad <= 3; grad += 0.25) {
//             let item = {
//               shipping_id : 118,
//               product_name : 'Biofinity Multifocal',
//               family : 'biof',
//               lens_type : 'mult',
//               graduation : grad.toFixed(2),
//               addition: '+2.50',
//               dominance: 'D',
//               box : 4,
//               box_available : 4,
//               blister : 4,
//               blister_available : 4
//             }
//             if (grad >= 0) {item.graduation = '+' + item.graduation}
//             biom_tor.push(item)
//         }
// console.log(biom_tor.length)

// biom_tor.forEach(item => {
//   connection.query('INSERT INTO shippings_detail SET ?', item, (error, result) => {
//     if (error) throw error;
//   })
// })

module.exports = cart