CREATE TABLE products (
    product_name varchar(40) NOT NULL UNIQUE,
    family varchar(20) NOT NULL,
    lens_type varchar(20) NOT NULL,
    graduations json,
    cylinders json,
    axes json,
    base_curves json,
    additions json,
    dominances json,
    diameters json,
    colors json,
    package JSON NOT NULL,
    PRIMARY KEY (family, lens_type),
);

CREATE TABLE biom_esf_stock (
    -- product_name varchar(40) NOT NULL,
    graduation varchar(6) NOT NULL,
    base_curve varchar(4) NOT NULL,
    box int (3),
    blister int (3)

    CHECK (box >= 0)
    CHECK (blister >= 0)
);

CREATE TABLE biom_tor_stock (
    graduation varchar(6) NOT NULL,
    cylinder varchar(5) NOT NULL,
    axis varchar(4) NOT NULL,
    box int(3),
    blister int (3)
    -- CHECK (box >= 0)
    -- CHECK (blister >= 0)
);

CREATE TABLE biof_esf_stock (
    graduation varchar(6) NOT NULL,
    box int(3),
    blister int (3)
    CHECK (box >= 0)
    CHECK (blister >= 0)
);

CREATE TABLE biof_tor_stock (
    graduation varchar(6) NOT NULL,
    cylinder varchar(5) NOT NULL,
    axis varchar(4) NOT NULL,
    box int(3),
    blister int (3)
    CHECK (box >= 0)
    CHECK (blister >= 0)
);

CREATE TABLE biofxr_esf_stock (
    graduation varchar(6) NOT NULL,
    box int(3),
    blister int (3)
    CHECK (box >= 0)
    CHECK (blister >= 0)
);

CREATE TABLE biofxr_tor_stock (
    graduation varchar(6) NOT NULL,
    cylinder varchar(5) NOT NULL,
    axis varchar(4) NOT NULL,
    box int(3),
    blister int (3)
    CHECK (box >= 0)
    CHECK (blister >= 0)
);

CREATE TABLE biof_mult_stock (
    graduation varchar(6) NOT NULL,
    addition varchar(5) NOT NULL,
    dominance varchar(3) NOT NULL,
    box int(3),
    blister int (3)
    CHECK (box >= 0)
    CHECK (blister >= 0)
);

CREATE TABLE proc_esf_stock (
    graduation varchar(6) NOT NULL,
    box int(3),
    blister int (3)
    CHECK (box >= 0)
    CHECK (blister >= 0)
);

CREATE TABLE proc_mult_stock (
    graduation varchar(6) NOT NULL,
    addition varchar(5) NOT NULL,
    dominance varchar(3) NOT NULL,
    box int(3),
    blister int (3)
    CHECK (box >= 0)
    CHECK (blister >= 0)
);

CREATE TABLE clar_esf_stock (
    graduation varchar(6) NOT NULL,
    box int(3),
    blister int (3)
    CHECK (box >= 0)
    CHECK (blister >= 0)
);

CREATE TABLE clar_mult_stock (
    graduation varchar(6) NOT NULL,
    addition varchar(5) NOT NULL,
    box int(3),
    blister int (3)
    CHECK (box >= 0)
    CHECK (blister >= 0)
);

CREATE TABLE hydr_tor_stock (
    graduation varchar(6) NOT NULL,
    cylinder varchar(5) NOT NULL,
    axis varchar(4) NOT NULL,
    box int(3),
    blister int (3)
    CHECK (box >= 0)
    CHECK (blister >= 0)
);

CREATE TABLE hydr_mult_stock (
    graduation varchar(6) NOT NULL,
    addition varchar(5) NOT NULL,
    box int(3),
    blister int (3)
    CHECK (box >= 0)
    CHECK (blister >= 0)
);

-- ***************** Other tables ***************** --

CREATE TABLE clients (
    account int(4) UNIQUE NOT NULL,
    business_name varchar(30) NOT NULL,
    nickname varchar(30) NOT NULL,
    --  shipping varchar(30),
    --  VER DE OTRA TABLA CON SHIPPINGS
    PRIMARY KEY (account)
);

CREATE TABLE orders (
    order_id int(8) UNIQUE NOT NULL AUTO_INCREMENT,
    client_account int(4) NOT NULL,
    contact varchar(30),
    notes text,
    order_date date NOT NULL,
    shipping_date date,
    shipping_type varchar(25) NOT NULL,
    order_type varchar(10) NOT NULL,
    deliver boolean default true,
    dispatched boolean default false,

    -- order_state varchar(15) DEFAULT 'pending',
    -- VER DE TABLA CON ESTADOS y poner un default

    FOREIGN KEY (client_account) REFERENCES clients(account),
    PRIMARY KEY (order_id)
);

CREATE TABLE orders_detail (
    id int (20) UNIQUE NOT NULL AUTO_INCREMENT,
    order_id int(8) NOT NULL,
    product_name varchar(40) NOT NULL ,
    family varchar(20) NOT NULL,
    lens_type varchar(20) NOT NULL,
    graduation varchar(6),
    cylinder varchar(20),
    axis varchar(4),
    base_curve varchar(4),
    addition varchar(5),
    dominance varchar(3),
    diameter varchar(6),
    color varchar(10),
    patient_case varchar(10),
    --client int(4) NOT NULL,
    box int(3) NOT NULL,
    blister int (3) NOT NULL,
    reserved varchar(5)

    -- CONSTRAINT CHK_Pack CHECK (box >= 0 AND blister >=0),
    FOREIGN KEY (order_id) REFERENCES orders(order_id),
    FOREIGN KEY (reserved) REFERENCES shippings(shippings_id),
    PRIMARY KEY (id)
);

CREATE TABLA shippings (
    shipping_id varchar(5) UNIQUE,
    estimated_arrival date,

    PRIMARY KEY (shipping_id)
);

CREATE TABLE shipping_detail (
    id int (20) UNIQUE NOT NULL AUTO_INCREMENT,
    shipping_id int NOT NULL,
    product_name varchar(40) NOT NULL ,
    family varchar(20) NOT NULL,
    lens_type varchar(20) NOT NULL,
    graduation varchar(6),
    cylinder varchar(20),
    axis varchar(4),
    base_curve varchar(4),
    addition varchar(5),
    dominance varchar(3),
    diameter varchar(6),
    color varchar(10),
    box int(3) NOT NULL,
    box_available int(3)
    blister int (3) NOT NULL,
    blister_available int(3),

    -- CONSTRAINT CHK_Pack CHECK (box >= 0 AND blister >=0),
    FOREIGN KEY (shipping_id) REFERENCES shippings(shipping_id),
    PRIMARY KEY (id)
);