export default 

    [
        {
            "graduation": "-0.00",
            "cylinder": "-0.75",
            "axis": "10°",
            "box": 1,
            "blister": 3
        },
        {
            
            "graduation": "-0.25",
            "cylinder": "-0.75",
            "axis": "10°",
            "box": 5,
            "blister": 2
        },
        {
            
            "graduation": "-0.50",
            "cylinder": "-0.75",
            "axis": "10°",
            "box": 3,
            "blister": 5
        },
        {
            
            "graduation": "-0.75",
            "cylinder": "-0.75",
            "axis": "10°",
            "box": 4,
            "blister": 3
        },
        {
            
            "graduation": "-1.00",
            "cylinder": "-0.75",
            "axis": "10°",
            "box": 4,
            "blister": 3
        },
        {
            
            "graduation": "-1.25",
            "cylinder": "-0.75",
            "axis": "10°",
            "box": 7,
            "blister": 3
        },
        {
            
            "graduation": "-1.50",
            "cylinder": "-0.75",
            "axis": "10°",
            "box": 0,
            "blister": 3
        },
        {
            
            "graduation": "-1.75",
            "cylinder": "-0.75",
            "axis": "10°",
            "box": 4,
            "blister": 3
        },
        {
            
            "graduation": "-2.00",
            "cylinder": "-0.75",
            "axis": "10°",
            "box": 6,
            "blister": 3
        },
        {
            
            "graduation": "-2.25",
            "cylinder": "-0.75",
            "axis": "10°",
            "box": 9,
            "blister": 3
        },
        {
            
            "graduation": "-2.50",
            "cylinder": "-0.75",
            "axis": "10°",
            "box": 6,
            "blister": 3
        },
        {
            
            "graduation": "-2.75",
            "cylinder": "-0.75",
            "axis": "10°",
            "box": 6,
            "blister": 3
        },
        {
            
            "graduation": "-3.00",
            "cylinder": "-0.75",
            "axis": "10°",
            "box": 0,
            "blister": 3
        },
        {
            
            "graduation": "-3.25",
            "cylinder": "-0.75",
            "axis": "10°",
            "box": 3,
            "blister": 3
        },
        {
            
            "graduation": "-3.50",
            "cylinder": "-0.75",
            "axis": "10°",
            "box": 1,
            "blister": 3
        },
        {
            
            "graduation": "-3.75",
            "cylinder": "-0.75",
            "axis": "10°",
            "box": 2,
            "blister": 3
        },
        {
            
            "graduation": "-4.00",
            "cylinder": "-0.75",
            "axis": "10°",
            "box": 6,
            "blister": 3
        },
        {
            
            "graduation": "-4.25",
            "cylinder": "-0.75",
            "axis": "10°",
            "box": 0,
            "blister": 3
        },
        {
            
            "graduation": "-4.50",
            "cylinder": "-0.75",
            "axis": "10°",
            "box": 0,
            "blister": 3
        },
        {
            
            "graduation": "-4.75",
            "cylinder": "-0.75",
            "axis": "10°",
            "box": 3,
            "blister": 3
        },  
        {
            
            "graduation": "-0.00",
            "cylinder": "-1.25",
            "axis": "10°",
            "box": 3,
            "blister": 3
        },  
        {
            
            "graduation": "-0.00",
            "cylinder": "-0.75",
            "axis": "20°",
            "box": 6,
            "blister": 4
        }  
    ]


// let biom_tor = []
// for (let cyl = -0.75; cyl>= -2.25 ; cyl -= 0.5) {
//     for (let eje = 10; eje <= 180; eje += 10) {
//         for (let grad = -10; grad <= 10; grad += 0.25) {
//             let item = {
//                 // product_name: 'biom_tor',
//                 graduation: grad.toFixed(2),
//                 cylinder: cyl.toFixed(2),
//                 axis: eje.toString() + '°',
//                 box: Math.floor(Math.random() * 16),
//                 blister:Math.floor(Math.random() * 16),
//             }
//             if (grad >= 0) {item.graduation = '+' + item.graduation}
//             biom_tor.push(item)
//         }
//     }
// }
// console.log(biom_tor)

// function updateStock(name, stock) {

//     const batch = firebase.firestore().batch();
//     let counter = 0
    
//     stock.forEach(async (product, index, arr) => {
//         if (counter === 500) {
//             counter = 0
//             await batch.commit().then(console.log)
//         }
//         let docId = name + '_' + (Number(product.graduation)*100) + (Number(product.cylinder)*100) + (Number(product.axis)*100)
//         let ref = firebase.firestore().collection(name).doc(docId)
    
//         batch.set(ref, product)

//         counter++
//         if ( index == (arr.length - 1) ) {
//             batch.commit().then(console.log)
//         }
    
//     })
// }

// function updateStock(name, stock) {

//     let counter = 0
//     let batch = []
    
//     stock.forEach( (product, index, arr) => {
//         if (counter === 500) {
//             counter = 0
//             console.log(batch)
//             batch = []
//         }
//         let docId = name + '_' + (Number(product.graduation)*100) + (Number(product.cylinder)*100) + Number(product.axis)
//         // let ref = firebase.firestore().collection(stock + '_stock').doc(docId)
//         product.id = docId
//         batch.push(product)
        
//         counter++
//         if ( index == (arr.length - 1) ) {
//             console.log(batch)
//         }
    
//     })
// }

// while(biom_tor.length) {
// 	const batch = firebase.firestore().batch()
// 		let batches = biom_tor.splice(0,500)
// 			.forEach(product => {
// 				let docId = 'biom_tor' + (Number(product.graduation)*100) + (Number(product.cylinder)*100) + Number(product.axis)
// 				let ref = firebase.firestore().collection('stock_biom_tor').doc(docId)
// 				batch.set(ref, product)
// 			});
// 			batch.commit().then(()=> console.log('Batched!'));
// }




// let biom_esf = []
//   for (let grad = 10; grad >= 0; grad += -0.25) {
//       let item = {
//           // product_name: 'biom_tor',
//           graduation: grad.toFixed(2),
//           base_curve: '-8.9',
//           box: Math.floor(Math.random() * 16),
//           blister:Math.floor(Math.random() * 16),
//       }
//       if (grad >= 0) {item.graduation = '+' + item.graduation}
//       biom_esf.push(item)
//   }

// biom_esf.forEach(item => {
//   connection.query('INSERT INTO biom_esf_stock SET ?', item, (error, result) => {
//     if (error) throw error;
//   })
// })